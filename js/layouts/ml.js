Keyboards.ml = {
  label: 'Malayalam',
  shortLabel: 'ml',
  imEngine: 'india',
  menuLabel: 'മലയാളം',
  secondLayout: true,
  alternateLayoutKey: '?123',
  basicLayoutKey: 'കഖഗ',
  types: ['text', 'url', 'email'],
  width: 10,
  keyClassName: 'malayalam',
  alt: {
    'ൗ':'ഔ',
    'ൈ':'ഐ',
    'ാ':'ആ',
    'ീ':'ഈ',
    'ൂ':'ഊ',

    'ോ':'ഓ',
    'േ':'ഏ',
    '്':'‌', // ZWNJ
    'ി':'ഇ',
    'ു':'ഉ',

    'ൊ':'ഒ',
    'െ':'എ',
    'ം':'ഃ',

    'ണ':'ൺ',
    'ര':'ർ',
    'റ':'ഋ',
    'ശ':'ൾ',
    'ക':'ൿ',
    'ന':'ൻ',
    'ല':'ൽ',
    'ള':'ൾ',
    'സ':'ശ'
  },
  upperCase: {
    'ൈ':'ൗ',
    'െ':'േ',
    'ര':'റ',
    'യ':'ഞ',
    'ത':'ഥ',
    'ു':'ൂ',
    'ി':'ീ',
    'ട':'ഠ',

    'ാ':'അ',
    'സ':'ഷ',
    'ദ':'ധ',
    'പ':'ഫ',
    'ഗ':'ഘ',
    'ഹ':'ഃ',

    'ബ':'ഭ',
    'ഹ':'ങ',


    'ജ':'ഝ',
    'ഴ':'ൃ',
    'ൃ':'ഋ',

    'ൊ':'ോ',
    'േ':'ഠ',
    '്':'',
    'ക':'ഖ',
    'ച':'ഛ',

    'മ':'ം',
    'ന':'ണ',
    'ല':'ള',
    'ഡ':'ഢ'
  },
  keys: [
    [
      { value: 'ൈ' }, { value: 'വ' }, { value: 'െ' }, { value: 'ര' },
      { value: 'ത' }, { value: 'യ' }, { value: 'ു' }, { value: 'ി' },
      { value: 'ൊ' }, { value: 'ട' }
    ], [
      { value: 'ാ' }, { value: 'സ' }, { value: 'ദ' }, { value: 'പ' }, { value: 'ഗ' },{ value: 'ഹ' },
      { value: 'ജ' }, { value: 'ക' }, { value: 'ല' }, { value: 'ഡ' }
    ], [
      { value: '⇪',ratio:1, keyCode: KeyEvent.DOM_VK_CAPS_LOCK },
      { value: 'ഴ' }, { value: '്' }, { value: 'ച' }, { value: 'വ' },
      { value: 'ബ' }, { value: 'ന' }, { value: 'മ' },
       { value: '⌫',ratio:1, keyCode: KeyEvent.DOM_VK_BACK_SPACE }
    ], [
      { value: '&nbsp', ratio: 9, keyCode: KeyboardEvent.DOM_VK_SPACE },
      { value: '↵', ratio: 1, keyCode: KeyEvent.DOM_VK_RETURN }
    ]
  ],
  alternateLayout: {
    alt: {
      '?': '¿',
      '!': '¡'
    },
    keys: [
      [
        { value: '1' }, { value: '2' }, { value: '3' } , { value: '4' },
        { value: '5' } , { value: '6' }, { value: '7' } , { value: '8' },
        { value: '9' }, { value: '0' }
      ], [
        { value: '@', hidden: ['email'] }, { value: '#' }, { value: '$' },
        { value: '%' }, { value: '&' } , { value: '*' }, { value: '-' },
        { value: '+' }, { value: '(' }, { value: ')' },
        { value: '_', visible: ['email'] }
      ], [
        { value: 'ALT', ratio: 1.5, keyCode: KeyEvent.DOM_VK_ALT },
        { value: '!' }, { value: '\"' }, { value: "'" }, { value: ':' },
        { value: ';' }, { value: '/' }, { value: '?' },
        { value: '⌫', ratio: 1.5, keyCode: KeyEvent.DOM_VK_BACK_SPACE }
      ], [
        { value: '&nbsp', ratio: 8, keyCode: KeyboardEvent.DOM_VK_SPACE },
        { value: '↵', ratio: 2, keyCode: KeyEvent.DOM_VK_RETURN }
      ]
    ]
  }
};
